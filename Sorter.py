from abc import ABC, abstractmethod
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import time

class Sorter(ABC):
    def __init__(self, array):
        plt.ion()
        self.array = array
        self.length = len(array)
        self.fig = plt.figure()
        self.ax1 = self.fig.add_subplot(1,1,1)


    @abstractmethod
    def sort(self):
        pass

    @abstractmethod
    def visualize_array(self, detail):
        pass

    def print_array(self):
        print(self.array)
