#!/usr/bin/python

import sys, getopt
import logging
from SortingFactory import SortingFactory

root = logging.getLogger()
handler = logging.StreamHandler(sys.stdout)


def print_usage(argv):
    print("[ Usage for Sort.py ]")
    print("EX: python3 Sort.py -s [sorting_algorithm] -a [x1,x2,...,xn]")
    print("-s\t Your choice of sorting algorithm")
    print("\t [ INSERT | QUICK ]")
    print("-a\t manual array input a1, a2, ..., an")
    print("-f\t file array input ")
    print("-v\t verbose option")
    print("-h\t display help text")
    sys.exit()

def main(argv):
    handler.setLevel(logging.DEBUG)
    try:
        opts, args = getopt.getopt(argv,"hf:s:a:",["file=", "sort=","array="])
    except getopt.GetoptError:
        print_usage(argv);
    array = []
    method = None

    for opt, arg in opts:
        if opt == '-h':
            print_usage(argv);
        elif opt in ("-a", "--array"):
            array = [int(x) for x in arg.split(",")]
        elif opt in ("-s", "--sort"):
            method = arg
        elif opt in ("-f", "--file"):
            logging.error("NOT IMPLEMENTED YET")
            sys.exit()

    if(len(array) == 0):
        logging.error("No array input.")
        print_usage(argv);

    factory = SortingFactory();
    sorter = factory.create_sorter(method, array)
    if sorter != None:
        sorter.sort()

if __name__ == "__main__":
   main(sys.argv[1:])
