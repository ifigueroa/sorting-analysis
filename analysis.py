import matplotlib.pyplot as plt
from quicksort import quicksort
import numpy as np
from numpy import random
import sys
import time

def generate_random_inputs():
    size = random.randint(10000);
    return random.randint(1000, size=(size))

def analyze_run():
    input = generate_random_inputs()
    size = len(input)
    start = time.time()
    quicksort(input, 0, size)
    run_time = time.time() - start
    return(size, run_time)

def analyze(num_samples):
    run_times = np.array([])
    run_size = np.array([])
    for i in range(0, num_samples):
        result = analyze_run();
        run_times = np.append(run_times, result[0])
        run_size = np.append(run_size, result[1])
    plt.scatter(run_times, run_size)
    plt.show()

analyze(100)
