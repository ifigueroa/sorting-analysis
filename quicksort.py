from Sorter import Sorter
import matplotlib.pyplot as plt

class QuickSort(Sorter):
    # def print_subset(array, left, right):
    #     for i in range(0, left):
    #         print(array[i], end=" ")
    #
    #     print("[", end=" ")
    #     for i in range(left, right):
    #         print(array[i], end=" ")
    #
    #     print("]", end=" ")
    #     for i in range(right, len(array)):
    #         print(array[i], end=" ")
    #     print()
    #
    # def print_breakdown(array, left, right, pivot):
    #     print("[", end=" ")
    #     for i in range(left, pivot):
    #         print(array[i], end=" ")
    #     print("]", end=" ")
    #
    #     for i in range(pivot, pivot+1):
    #         print(array[i], end=" ")
    #
    #     print("[", end=" ")
    #     for i in range(pivot+1, right):
    #         print(array[i], end=" ")
    #     print("]")

    def swap(self, i, j):
        self.array[i], self.array[j] = self.array[j], self.array[i]

    def partition_last_element(self, left, right):
        pivot = self.array[right-1]
        # print("Pivot: ", pivot)
        i= left-1
        for j in range(left, right-1):
            if(self.array[j] <= pivot):
                i+=1
                self.swap( i, j)
            # print_subset(array, left,right)
        self.swap(i+1, right-1)
        # print_subset(array, left,right)
        # print("Pivot Loc: ", i+1)
        return i+1;

    def quicksort(self, left, right):
        if(left < right):
            # print("Quicksort Call -------------------------")
            p = self.partition_last_element(left, right)
            detail = (left, right, p)
            self.visualize_array(detail)
            # print("Next Arrays: ", end="")
            # print_breakdown(array, left, right, p)
            self.quicksort(left, p)
            self.quicksort(p+1, right)

    # Abstract Method Implementation

    def sort(self):
        self.quicksort(0, self.length)

    def visualize_array(self, detail):
        (left, right, p) = detail
        print(p)
        self.ax1.clear()
        x = [ i for i in range(0, self.length)]
        #Sorted
        #unsorted
        #pivot
        self.ax1.bar(x, self.array, color="tab:blue")

        # self.ax1.bar(x[p:p+1], self.array[p:p+1])
        self.ax1.bar(x[left:p], self.array[left:p], color='red')
        self.ax1.bar(x[p+1:right], self.array[p+1:right], color='tab:orange')
        self.ax1.bar(x[right:self.length], self.array[right:self.length],  color='red')

        self.ax1.set_title("Quick Sort")
        self.ax1.set_xlabel("Index")
        self.ax1.set_ylabel("Value")
        plt.pause(1)

Q = QuickSort([1,2,99,6, -5, 10, 6, 1, 3]);
Q.sort()
Q.print_array()
