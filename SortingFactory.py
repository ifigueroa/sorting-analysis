from InsertionSort import InsertionSort
from QuickSort import QuickSort

class SortingFactory:
    def create_sorter(self, algorithm, array):
        if algorithm == "INSERT":
            return InsertionSort(array)
        elif algorithm == "QUICK":
            return QuickSort(array)
        else:
            return None
