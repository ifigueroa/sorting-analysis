from Sorter import Sorter
import matplotlib.pyplot as plt
# import time

class InsertionSort(Sorter):

    def insert(self, i):
        key = self.array[i]
        key_idx = i
        swap_idx = i-1
        while(self.array[swap_idx] > key and swap_idx >= 0):
            self.array[swap_idx], self.array[key_idx] = self.array[key_idx], self.array[swap_idx]
            swap_idx -=1
            key_idx -=1
            self.visualize_array(key_idx)


    def insertionSort(self):
        for i in range(1, self.length):
            self.insert(i)

    # Abstract Method Implementation
    def sort(self):
        self.insertionSort();

    def visualize_array(self, curr_idx):
        self.ax1.clear()
        x = [ i for i in range(0, self.length)]
        self.ax1.bar(x, self.array)
        self.ax1.bar(curr_idx, self.array[curr_idx])
        self.ax1.set_title("Insertion Sort")
        self.ax1.set_xlabel("Index")
        self.ax1.set_ylabel("Value")
        plt.pause(0.5)


I = InsertionSort([1,2,99,6, 0, -5, 10, 6, 1, 3]);
I.sort();
