import unittest

from quicksort import quicksort

class Testing(unittest.TestCase):
    def test_quicksort(self):
        array = [3,1,7,8,4,2,10,9,5]
        expected = [1, 2, 3, 4, 5, 7, 8, 9, 10]
        result = quicksort(array, 0, len(array))
        self.assertEqual(expected, result)
    def test_quicksort_empty(self):
        array = []
        expected = []
        result = quicksort(array, 0, len(array))
        self.assertEqual(expected, result)

if __name__ == '__main__':
    unittest.main()
